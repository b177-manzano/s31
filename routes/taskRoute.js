const express =require ("express");
// Create a Rounter instance that functions as a routing system
const router = express.Router();
//Import the taskControllers
const taskController =require ("../controllers/taskControllers") ;

//Route to get all the tasks
// endpoint: localhost:3001/task
router.get("/" , (req,res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

//Route to create a tasks
router.post("/", (req,res) => {
	taskController.createTask(req.body).then (resultFromController => res.send(resultFromController));
})

//Route to DELETE a tasks
// endpoint localhost:3000/tasks/123456
router.delete("/:id", (req,res) => {
	taskController.deleteTask(req.params.id).then (resultFromController => res.send(resultFromController));
})

//Route to Update a task
router.put("/:id", (req,res) => {
	taskController.updateTask(req.params.id , req.body).then(resultFromController => res.send(resultFromController));
})

//Activity
//2 
router.get("/:id", (req,res) => {
	taskController.findTask(req.params.id).then (resultFromController => res.send(resultFromController));
})

//
router.put("/:id/complete", (req,res) => {
    taskController.updateTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


// export the router object to be used in index.js
module.exports = router;